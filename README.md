<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

# openDesk build asset generator

A small script that creates index files in JSON format the following artefacts used in the [openDesk deployment](https://gitlab.opencode.de/bmi/souveraener_arbeitsplatz/deployment/sovereign-workplace):
- Helm charts
- container images

# Other options

If you want to verify the list of container images from the script-generated JSON you could do that by checking the containers that are running in a given namespace:

```
kubectl get pods -n <your_namespace_here> -o jsonpath="{.items[*].spec.containers[*].image}" | tr -s '[[:space:]]' '\n' | sort | uniq -c
```
